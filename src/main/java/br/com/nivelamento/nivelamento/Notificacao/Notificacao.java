package br.com.nivelamento.nivelamento.Notificacao;


import br.com.nivelamento.nivelamento.DTO.RespostaDTO;
import br.com.nivelamento.nivelamento.Protocolo.Protocolo;
import br.com.nivelamento.nivelamento.exessao.ProdutoNaoEncontradoPorIdException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class Notificacao {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public RespostaDTO<String> produtoInvalido(MethodArgumentNotValidException e){

        return new RespostaDTO<>(Protocolo.PRODUTO_INVALIDO, e.getMessage());

    }


    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ProdutoNaoEncontradoPorIdException.class)
    public RespostaDTO<String> produtoNaoEncontrado(ProdutoNaoEncontradoPorIdException p) {
        return new RespostaDTO<>(p.getProtocolo(), p.getMessage());

    }
}
