package br.com.nivelamento.nivelamento.repositorio;

import br.com.nivelamento.nivelamento.modelo.Produto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Repositorio extends JpaRepository<Produto, Long> {

}
