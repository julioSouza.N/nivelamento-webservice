package br.com.nivelamento.nivelamento.Protocolo;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Protocolo {


    PRODUTO_NAO_EXISTE(40004,"Produto nao foi encontrado"),
    PRODUTO_INVALIDO(20002,"Produto invalido");


    private final Integer valor;
    private final String descricao;

}
