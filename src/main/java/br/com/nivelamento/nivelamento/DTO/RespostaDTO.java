package br.com.nivelamento.nivelamento.DTO;

import br.com.nivelamento.nivelamento.Protocolo.Protocolo;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class RespostaDTO<T> {

    private LocalDateTime dataAtual;

    private Integer codigo;

    private String descricao;

    private T mensagem;


    public  RespostaDTO(Protocolo protocolo, T mensagem){


        this.dataAtual = LocalDateTime.now();
        this.codigo = protocolo.getValor();
        this.descricao = protocolo.getDescricao();
        this.mensagem = mensagem;

    }


    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    public LocalDateTime getDataAtual(){

        return dataAtual;

    }
}
