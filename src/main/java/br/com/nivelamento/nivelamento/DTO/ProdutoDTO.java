package br.com.nivelamento.nivelamento.DTO;


import lombok.Data;

import javax.validation.constraints.*;

@Data
public class ProdutoDTO {

    @NotEmpty
    private String nome;

    @NotNull
    @Min(0)
    private Integer quantidadeDisponivel;

    @NotNull
    @Min(0)
    @Max(10)
    private Integer status;

    @NotNull
    @Min(0)
    @Max(99)
    private Integer desconto;

    @NotNull
    @Min(0)
    private Double preço;


}
