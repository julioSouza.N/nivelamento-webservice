package br.com.nivelamento.nivelamento.modelo;


import br.com.nivelamento.nivelamento.DTO.ProdutoDTO;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "produto")
@Getter
@Setter
@NoArgsConstructor
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotEmpty
    private String nome;


    private int quantidadeDisponivel;


    private int status;

    private int desconto;


    private double preço;




    public Produto(ProdutoDTO produtoDTO){

            this.nome = produtoDTO.getNome();
            this.quantidadeDisponivel = produtoDTO.getQuantidadeDisponivel();
            this.desconto = produtoDTO.getDesconto();
            this.status =  produtoDTO.getStatus();
            this.preço = produtoDTO.getPreço();


    }

    public void arualizarProduto(ProdutoDTO produtoDTO){

        this.nome = produtoDTO.getNome();
        this.quantidadeDisponivel = produtoDTO.getQuantidadeDisponivel();
        this.desconto = produtoDTO.getDesconto();
        this.status =  produtoDTO.getStatus();
        this.preço = produtoDTO.getPreço();

    }
}
