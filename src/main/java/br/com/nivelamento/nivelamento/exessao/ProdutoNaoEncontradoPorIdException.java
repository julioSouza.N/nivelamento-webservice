package br.com.nivelamento.nivelamento.exessao;


import br.com.nivelamento.nivelamento.Protocolo.Protocolo;
import lombok.Getter;

@Getter
public class ProdutoNaoEncontradoPorIdException extends RuntimeException{

    private final Protocolo protocolo;



    public ProdutoNaoEncontradoPorIdException (String message){

        super(message);
        this.protocolo = Protocolo.PRODUTO_NAO_EXISTE;

    }


}
