package br.com.nivelamento.nivelamento.controler;


import br.com.nivelamento.nivelamento.DTO.ProdutoDTO;
import br.com.nivelamento.nivelamento.modelo.Produto;
import br.com.nivelamento.nivelamento.servico.Servico;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.util.Collection;

//TODO resolver mensagem de erro

@Slf4j
@RestController
public class Controle {

    private final Servico servico;

    public Controle(Servico servico){
        this.servico=servico;

    }


    @GetMapping
    public Collection<Produto> obterTodosProdutos() {

        log.info("recebi uma requisição do cliente para obter todos os produtos");


         Collection<Produto> produtos = servico.obterProdutos();

        log.info("retornando tudos so produtos");

        return produtos;
    }



    @GetMapping("/{id}")
    public Produto obterPorId(@PathVariable("id") Long id) {

        log.info("Recebendo um id para buscar um produto"+id);

        return servico.obierProdutoId(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Produto registarProduto(@RequestBody @Validated  ProdutoDTO produtoDTO){


        log.info("Requisição para registar um produto novo");

        Produto produtoRegistrado = servico.registarproduto(produtoDTO);

        log.info("Produto registarado");

        return produtoRegistrado;

    }



    @DeleteMapping("/{id}")
    public Produto removerProduto(@PathVariable long id ){

        log.info("Recebendo um id para deletar um produto"+id);

        Produto produtoRegistrado = servico.deletarProduto(id);

        log.info("Produto deletado");

        return produtoRegistrado;

    }



    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable long id ,@RequestBody @Validated  ProdutoDTO produtoDTO){

        log.info("Recebendo um id para atualizar um produto"+id);

        Produto produtoa =servico.autalizarProduto(id,produtoDTO);

        log.info("Produto atualizado "+id);

        return produtoa;

    }



}
