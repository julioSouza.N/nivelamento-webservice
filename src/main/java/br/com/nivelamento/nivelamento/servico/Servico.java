package br.com.nivelamento.nivelamento.servico;


import br.com.nivelamento.nivelamento.DTO.ProdutoDTO;
import br.com.nivelamento.nivelamento.exessao.ProdutoNaoEncontradoPorIdException;
import br.com.nivelamento.nivelamento.modelo.Produto;
import br.com.nivelamento.nivelamento.repositorio.Repositorio;
import lombok.Setter;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.Optional;

@Service
public class Servico {


    private final Repositorio repositorio;

    public Servico(Repositorio repositorio) {
        this.repositorio = repositorio;
    }


    public Produto registarproduto(ProdutoDTO produtoDTO){

        Produto produto = new Produto(produtoDTO);

        return repositorio.save(produto);

    }


    public Collection<Produto> obterProdutos(){

        return repositorio.findAll();


    }


    public Produto deletarProduto(long id){


        Produto produto = getProduto(id);

        repositorio.deleteById(id);

        return produto;
    }


    public Produto obierProdutoId(long id){


        Produto produto = getProduto(id);

        return  produto;

    }


    public Produto autalizarProduto(long id,ProdutoDTO produtoDTO){

        Produto produto =  getProduto(id);

        produto.arualizarProduto(produtoDTO);

        repositorio.save(produto);

        return produto;
    }

    private Produto getProduto(Long id){

        return repositorio.findById(id).orElseThrow(()-> new ProdutoNaoEncontradoPorIdException(
                "produto com id '{}'".replace("{}",id.toString())
        ));


    }

}
