package br.com.nivelamento.nivelamento.Util;


import com.google.gson.GsonBuilder;

public interface GsonUtils {

    static String objectToString(Object object){

        return new GsonBuilder().create().toJson(object);

    }
}
