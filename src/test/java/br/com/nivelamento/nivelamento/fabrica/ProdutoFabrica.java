package br.com.nivelamento.nivelamento.fabrica;

import br.com.nivelamento.nivelamento.modelo.Produto;

public interface ProdutoFabrica {


    static Produto criar(String nome){

        Produto novopProduto = new Produto();

        novopProduto.setDesconto(80);
        novopProduto.setNome(nome);
        novopProduto.setQuantidadeDisponivel(100);
        novopProduto.setStatus(0);
        novopProduto.setPreço(45f);
        return novopProduto;
    }


}
