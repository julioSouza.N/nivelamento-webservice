package br.com.nivelamento.nivelamento.controle;

import br.com.nivelamento.nivelamento.Protocolo.Protocolo;
import br.com.nivelamento.nivelamento.Util.GsonUtils;
import br.com.nivelamento.nivelamento.fabrica.ProdutoFabrica;
import br.com.nivelamento.nivelamento.modelo.Produto;
import jdk.net.SocketFlow;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProdutoTest {

    private static final String MENSAGEM_ID = "$.id";
    private static final String MENSAGEM_NOME = "$.nome";
    private static final String CODIGO  = "$.codigo";
    private static final String CAMERA  = "CAMERA";
    private static final String CELULAR = "CELULAR";
    private static final String TEQUILA = "TEQUILA";

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void cadastrarProdutoCameraTest() throws Exception {

        final Produto produto = ProdutoFabrica.criar(CAMERA);

        mockMvc
                .perform(MockMvcRequestBuilders
                            .post("")
                            .contentType(MediaType.APPLICATION_JSON_UTF8)
                            .content(GsonUtils.objectToString(produto))).
                andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath(MENSAGEM_ID).value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath(MENSAGEM_NOME).value(CAMERA));

    }



    @Test
    public void cadastrarProdutoCelularTest() throws Exception {

        final Produto produto = ProdutoFabrica.criar(CELULAR);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(GsonUtils.objectToString(produto))).
                andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath(MENSAGEM_ID).value(2L))
                .andExpect(MockMvcResultMatchers.jsonPath(MENSAGEM_NOME).value(CELULAR));

    }

    @Test
    public void cadastarProdutoInvalidoTeste() throws Exception {

        final Produto produto = ProdutoFabrica.criar(null);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(GsonUtils.objectToString(produto)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath(CODIGO).value(Protocolo.PRODUTO_INVALIDO.getValor()));

    }


    @Test
    public void listarProdutosCadastradosTest() throws Exception {

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(""))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].nome").value(CAMERA))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(2L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[1].nome").value(CELULAR));
    }

    @Test
    public void obterProdutoPorIdTest() throws Exception {

        final Long idProduto = 2L;

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/"+idProduto.toString()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(MENSAGEM_ID).value(idProduto))
                .andExpect(MockMvcResultMatchers.jsonPath(MENSAGEM_NOME).value(CELULAR));

    }


    @Test
    public void obterProdutoPorIdInexistenteTest() throws Exception {

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/0"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath(CODIGO).value(Protocolo.PRODUTO_NAO_EXISTE.getValor()));


    }


    @Test
    public void updateProdutoProIdvalidoTest() throws Exception {

        final Long idProduto = 2L;
        final Produto produto = ProdutoFabrica.criar(TEQUILA);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put("/"+ idProduto.toString())
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(GsonUtils.objectToString(produto)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(MENSAGEM_ID).value(idProduto))
                .andExpect(MockMvcResultMatchers.jsonPath(MENSAGEM_NOME).value(TEQUILA));


    }


    @Test
    public void updateProdutoProIdInexistenetTest() throws Exception {


        final Produto produto = ProdutoFabrica.criar(TEQUILA);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .put("/0")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(GsonUtils.objectToString(produto)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath(CODIGO).value(Protocolo.PRODUTO_NAO_EXISTE.getValor()));

    }


    @Test
    public void updateProdutoInvalidoTeste() throws Exception {

        final Produto produto = ProdutoFabrica.criar(null);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/0")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(GsonUtils.objectToString(produto)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath(CODIGO).value(Protocolo.PRODUTO_INVALIDO.getValor()));

    }

    @Test
    public void updatelistarProdutosTest() throws Exception {

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(""))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(2L))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[1].nome").value(TEQUILA));
    }

    @Test
    public void removerProdutoPorId() throws Exception {

       final Long idProduto = 1L;


        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/"+ idProduto.toString()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(MENSAGEM_ID).value(idProduto))
                .andExpect(MockMvcResultMatchers.jsonPath(MENSAGEM_NOME).value(CAMERA));
    }


    @Test
    public void removerProdutoPorInexistenteId() throws Exception {

        mockMvc
                .perform(MockMvcRequestBuilders
                        .delete("/0"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath(CODIGO).value(Protocolo.PRODUTO_NAO_EXISTE.getValor()));


    }





}
